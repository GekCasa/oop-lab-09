package it.unibo.oop.lab.workers02;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MultiThreadedSumMatrix implements SumMatrix {
    
    private final int nthread;
    

    public MultiThreadedSumMatrix(final int nthread) {
        this.nthread = nthread;
    }
    
    private static class Worker extends Thread {
        private final List<Double> list;
        private final int startpos;
        private final int nelem;
        private double res;

        /**
         * Build a new worker.
         * 
         * @param list
         *            the list to sum
         * @param startpos
         *            the initial position for this worker
         * @param nelem
         *            the no. of elems to sum up for this worker
         */
        Worker(final List<Double> list, final int startpos, final int nelem) {
            super();
            this.list = list;
            this.startpos = startpos;
            this.nelem = nelem;
        }

        @Override
        public void run() {
            System.out.println("Working from position " + startpos + " to position " + (startpos + nelem - 1));
            for (int i = startpos; i < list.size() && i < startpos + nelem; i++) {
                this.res += this.list.get(i);
            }
        }

        /**
         * Returns the risult of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public double getResult() {
            return this.res;
        }

    }


    @Override
    public double sum(final double[][] matrix) {
        final List<double[]> bucket = Arrays.asList(matrix);
        final List<Double> trueList = new ArrayList<>();
        List<Double> kappa = new ArrayList<>(); 
        for(final double[] prop : bucket) {
           Arrays.asList(prop);
            
        }
        final int size = trueList.size() % nthread + trueList.size() / nthread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = new ArrayList<>(nthread);
        for (int start = 0; start < trueList.size(); start += size) {
            workers.add(new Worker(trueList, start, size));
        }
        /*
         * Start them
         */
        for (final Worker w: workers) {
            w.start();
        }
        /*
         * Wait for every one of them to finish. This operation is _way_ better done by
         * using barriers and latches, and the whole operation would be better done with
         * futures.
         */
        long sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
        return sum;
    }
        
}
